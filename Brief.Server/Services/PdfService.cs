﻿using Brief.Server.Data.Types.Brief;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Brief.Server.Services;

public static class PdfService
{
    public static string GetBriefHtml(BriefInfo briefInfo)
    {
        var dataJson = JObject.Parse(briefInfo.Data);

        var sb = new StringBuilder();
        sb.Append(@"
            <html>
                <head>
                </head>
                <body>
                    <div class='header'><h1>Brief</h1></div>");

        sb.Append($"<div class='created-date'>Created date:{briefInfo.CreatedDate}</div>");
        sb.Append($"<div class='last-modified-date'>Last time modified date: {briefInfo.LastModifiedDate}</div>");

        sb.Append(@"
                    <ul>");
        foreach (var property in dataJson.Properties())
        {
            HandleObject(property);
        }
        sb.Append(@"
                    </ul>");

        sb.Append(@"
                </body>
            </html>");

        return sb.ToString();

        void HandleObject(JProperty property)
        {
            sb.AppendFormat(@"
                        <li>{0}: ", property.Name);
            if (property.Value.Type == JTokenType.Object)
            {
                sb.Append("<ul>");
                foreach (JProperty childProperty in property.Value)
                {
                    HandleObject(childProperty);
                }
                sb.Append("</ul></li>");
            }
            else if (property.Value.Type == JTokenType.Array)
            {
                sb.Append("<ul>");
                foreach (var item in property.Value)
                {
                    sb.AppendFormat("<li>{0}</li>", item);
                }
                sb.Append("</ul></li>");
            }
            else
            {
                sb.AppendFormat("{0}</li>", property.Value);
            }
        }
    }

}
