﻿using Brief.Server.Data.Types.User;

using Microsoft.AspNetCore.Authentication.Cookies;

using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Brief.Server.Services;

public class AuthService
{
    const int KeySize = 64;
    const int Iterations = 350_000;
    readonly HashAlgorithmName HashAlgorithm = HashAlgorithmName.SHA256;

    public (string, byte[]) HashPassword(string password)
    {
        var salt = RandomNumberGenerator.GetBytes(KeySize);

        var hash = Rfc2898DeriveBytes.Pbkdf2(
            Encoding.UTF8.GetBytes(password),
            salt,
            Iterations,
            HashAlgorithm,
            KeySize);

        return (Convert.ToHexString(hash), salt);
    }

    public bool VerifyPassword(string password, string hashPassword, byte[] salt)
    {
        var hashToCompare = Rfc2898DeriveBytes.Pbkdf2(password, salt, Iterations, HashAlgorithm, KeySize);
        var i = Convert.ToHexString(hashToCompare);
        return CryptographicOperations.FixedTimeEquals(hashToCompare, Convert.FromHexString(hashPassword));
    }

    public ClaimsPrincipal GetClaimsPrincipal(User user)
    {
        var claims = new List<Claim>
        {
            new(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new(ClaimTypes.Name, user.Login),
        };

        var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
        return new ClaimsPrincipal(claimsIdentity);
    }

    public Guid GetUserId(ClaimsPrincipal claimsPrincipal)
    {
        var id = claimsPrincipal.FindFirstValue(ClaimTypes.NameIdentifier);

        if (id is null)
            throw new ArgumentNullException(id, "'NameIdentifier' cannot be null");

        return new Guid(id);
    }
}