﻿using Brief.Server.Data.Repositories;
using Brief.Server.Data.Types.User;

using System.Security.Claims;

namespace Brief.Server.Services;

public class UserService(AuthService authService, UserRepository userRepository)
{
    readonly AuthService authService = authService;
    readonly UserRepository userRepository = userRepository;

    public async Task<(bool, User?, ClaimsPrincipal?)> AuthenticateUser(LoginInput loginInput)
    {
        var user = await userRepository.GetUser(loginInput.Login);
        if (user == null || !authService.VerifyPassword(loginInput.Password, user.HashedPassword, user.Salt))
            return (false, null, null);

        return (true, user, authService.GetClaimsPrincipal(user));
    }

    public async Task<User> RefreshUser(ClaimsPrincipal claimsPrincipal)
    {
        var userId = authService.GetUserId(claimsPrincipal);
        return await userRepository.GetUser(userId);
    }
}
