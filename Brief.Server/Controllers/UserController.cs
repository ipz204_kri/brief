﻿using Brief.Server.Data.Types.User;
using Brief.Server.Services;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Net.Mime;

namespace Brief.Server.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController(UserService userService) : Controller
{
    readonly UserService userService = userService;

    const string AuthenticationScheme = CookieAuthenticationDefaults.AuthenticationScheme;

    [HttpPost()]
    [Route("Login")]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Login([FromBody] LoginInput input)
    {
        try
        {
            var (success, user, claimsPrincipal) = await userService.AuthenticateUser(input);

            if (!success)
                return Unauthorized();

            await HttpContext.SignInAsync(
                AuthenticationScheme,
                claimsPrincipal!,
                new AuthenticationProperties
                {
                    AllowRefresh = true,
                    IsPersistent = false,
                });

            return Ok(new UserResponse(user!));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost()]
    [Route("RefreshUser")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> RefreshUser()
    {
        try
        {
            var result = await HttpContext.AuthenticateAsync(AuthenticationScheme);

            if (!result.Succeeded)
                return Unauthorized();

            var user = await userService.RefreshUser(result.Principal);

            return Ok(new UserResponse(user));
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }

    [HttpPost()]
    [Route("Logout")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> Logout()
    {
        try
        {
            await HttpContext.SignOutAsync(AuthenticationScheme);

            return Ok();
        }
        catch (Exception ex)
        {
            return BadRequest(ex.Message);
        }
    }
}
