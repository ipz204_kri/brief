using Brief.Server.Data.Repositories;
using Brief.Server.Data.Types.Brief;
using Brief.Server.Services;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Net.Mime;
using System.Text.Json;

namespace Brief.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BriefInfoController(BriefRepository repository, IConverter pdfConverter) : ControllerBase
    {
        readonly BriefRepository repository = repository;
        readonly IConverter pdfConverter = pdfConverter;

        [HttpGet()]
        [Route("GetBriefsInfo")]
        [Authorize]
        [ProducesResponseType<BriefInfo>(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<BriefInfo>>> GetBriefsInfo()
        {
            try
            {
                var briefsInfo = await repository.GetBriefsAsync();
                return Ok(briefsInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet()]
        [Route("GetBriefInfo")]
        [Authorize]
        [ProducesResponseType<BriefInfo>(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<BriefInfo>>> GetBriefInfo(string id)
        {
            try
            {
                var briefInfo = await repository.GetBriefAsync(new(id));
                return Ok(briefInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost()]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostBriefInfo([FromBody] object brief)
        {
            try
            {
                var briefInfo = new BriefInfo(JsonSerializer.Serialize(brief));
                await repository.SaveBriefAsync(briefInfo);
                return CreatedAtAction(nameof(PostBriefInfo), briefInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("EditBriefInfo")]
        [Authorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EditBriefInfo([FromBody] EditBriefInput editedBriefData)
        {
            try
            {
                var briefInfo = await repository.GetBriefAsync(new(editedBriefData.Id));

                if (briefInfo is null)
                    return NotFound();

                briefInfo.Data = JsonSerializer.Serialize(editedBriefData.Data);
                briefInfo.LastModifiedDate = DateTime.UtcNow;
                await repository.EditBriefAsync(briefInfo);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Authorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteBriefInfo([FromBody] IdBriefInput deleteBriefInput)
        {
            try
            {
                await repository.DeleteBriefAsync(new Guid(deleteBriefInput.BriefId));
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize]
        [Route("SaveAsPdf")]
        [ProducesResponseType<BriefInfo>(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> SaveAsPdf([FromBody] IdBriefInput input)
        {
            try
            {
                var briefInfo = await repository.GetBriefAsync(new(input.BriefId));

                if (briefInfo is null)
                    return NotFound();

                var globalSettings = new GlobalSettings
                {
                    ColorMode = ColorMode.Color,
                    Orientation = Orientation.Portrait,
                    PaperSize = PaperKind.A4,
                    Margins = new MarginSettings { Top = 10 },
                    DocumentTitle = $"Brief Result {briefInfo.Id}",
                };

                var objectSettings = new ObjectSettings
                {
                    PagesCount = true,
                    HtmlContent = PdfService.GetBriefHtml(briefInfo),
                    WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "Assets", "PdfPage.css") },
                };

                var pdf = new HtmlToPdfDocument()
                {
                    GlobalSettings = globalSettings,
                    Objects = { objectSettings }
                };

                var file = pdfConverter.Convert(pdf);
                return File(file, "application/pdf", "EmployeeReport.pdf");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public record EditBriefInput(string Id, object Data);

        public record IdBriefInput(string BriefId);
    }
}
