﻿using Microsoft.Data.SqlClient;

using System.Data;

namespace Brief.Server.Data;

public class DapperContext(IConfiguration configuration)
{
    const string ConnectionStringName = "BriefDBConnection";

    readonly string connectionString = configuration.GetConnectionString(ConnectionStringName) ??
            throw new ArgumentNullException(null, "Connection string is null.");

    public IDbConnection CreateConnection() => new SqlConnection(connectionString);
}
