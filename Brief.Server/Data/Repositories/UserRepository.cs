﻿using Brief.Server.Data.Types.User;

using Dapper;

namespace Brief.Server.Data.Repositories;

public class UserRepository(DapperContext dapperContext) : Repository(dapperContext, "Users")
{
    public async Task<User?> GetUser(string login)
    {
        var query = $"SELECT * FROM {TableName} " +
            $"WHERE [{nameof(User.Login)}] = @login";

        using var connection = dapperContext.CreateConnection();

        return await connection.QuerySingleOrDefaultAsync<User>(query, new { login });
    }

    public async Task<User> GetUser(Guid id)
    {
        var query = $"SELECT * FROM {TableName} " +
            $"WHERE [{nameof(User.Id)}] = @id";

        using var connection = dapperContext.CreateConnection();

        return await connection.QuerySingleAsync<User>(query, new { id });
    }
}
