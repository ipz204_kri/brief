﻿using Brief.Server.Data.Types.Brief;

using Dapper;

namespace Brief.Server.Data.Repositories;

public class BriefRepository(DapperContext dapperContext) : Repository(dapperContext, "BriefData")
{
    public async Task<BriefInfo?> GetBriefAsync(Guid id)
    {
        var query = $"SELECT * FROM {TableName} WHERE [{nameof(BriefInfo.Id)}] = @id";

        using var connection = dapperContext.CreateConnection();
        var res = await connection.QuerySingleOrDefaultAsync<BriefInfo>(query, new { id });

        return res;
    }

    public async Task<IEnumerable<BriefInfo>> GetBriefsAsync()
    {
        var query = $"SELECT * FROM {TableName} ORDER BY [{nameof(BriefInfo.CreatedDate)}]";

        using var connection = dapperContext.CreateConnection();
        var res = await connection.QueryAsync<BriefInfo>(query);

        return res;
    }

    public async Task SaveBriefAsync(BriefInfo briefInfo)
    {
        var query = $"INSERT INTO {TableName} " +
            $"({nameof(BriefInfo.Id)}, " +
            $"{nameof(BriefInfo.Data)}, " +
            $"{nameof(BriefInfo.CreatedDate)}, " +
            $"{nameof(BriefInfo.LastModifiedDate)}) " +
            $"VALUES " +
            $"(@Id, @Data, @CreatedDate, @LastModifiedDate);";

        using var connection = dapperContext.CreateConnection();
        await connection.ExecuteAsync(query, briefInfo);
    }

    public async Task EditBriefAsync(BriefInfo briefInfo)
    {
        var query = $"UPDATE {TableName} SET " +
            $"{nameof(BriefInfo.Data)} = @Data, " +
            $"{nameof(BriefInfo.LastModifiedDate)} = @LastModifiedDate " +
            $"WHERE {nameof(BriefInfo.Id)} = @Id;";

        using var connection = dapperContext.CreateConnection();
        await connection.ExecuteAsync(query, briefInfo);
    }

    public async Task DeleteBriefAsync(Guid id)
    {
        var query = $"DELETE FROM {TableName} WHERE " +
            $"{nameof(BriefInfo.Id)} = @id";

        using var connection = dapperContext.CreateConnection();
        await connection.ExecuteAsync(query, new { id });
    }
}
