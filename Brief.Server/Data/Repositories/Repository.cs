﻿namespace Brief.Server.Data.Repositories;

public abstract class Repository(DapperContext dapperContext, string tableName)
{
    protected readonly DapperContext dapperContext = dapperContext;

    protected readonly string TableName = $"[{tableName}]";
}