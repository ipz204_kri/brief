﻿using System.ComponentModel.DataAnnotations;

namespace Brief.Server.Data.Types.Brief;

public class BriefInfo
{
    public Guid Id { get; set; }

    [Required]
    public string Data { get; set; }

    public DateTime CreatedDate { get; set; }

    public DateTime LastModifiedDate { get; set; }

    public BriefInfo()
    {
        Data = "";
    }

    public BriefInfo(string briefData)
    {
        Id = Guid.NewGuid();
        Data = briefData;
        CreatedDate = LastModifiedDate = DateTime.UtcNow;
    }
}
