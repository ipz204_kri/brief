﻿using System.ComponentModel.DataAnnotations;

namespace Brief.Server.Data.Types.User;

public class LoginInput(string login, string password)
{
    [Required]
    public string Login { get; set; } = login;

    [Required]
    public string Password { get; set; } = password;
}
