﻿namespace Brief.Server.Data.Types.User;

public class UserResponse(User user)
{
    public Guid Id { get; set; } = user.Id;

    public string Login { get; set; } = user.Login;
}
