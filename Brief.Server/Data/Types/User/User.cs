﻿namespace Brief.Server.Data.Types.User;

public class User(Guid id, string login, string hashedPassword, byte[] salt)
{
    public Guid Id { get; set; } = id;

    public string Login { get; set; } = login;

    public string HashedPassword { get; init; } = hashedPassword;

    public byte[] Salt { get; init; } = salt;
}
