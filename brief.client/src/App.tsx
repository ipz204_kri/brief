import './App.css';
import Layout from './components/layout/Layout';
import NotFoundPage from './components/layout/NotFoundPage';
import BriefForm from './components/brief/briefForm/BriefForm';
import { WebRoutes } from './behavior/routes';
import { Routes, Route } from 'react-router-dom';
import Home from './components/home/Home';
import BriefsList from './components/brief/briefList/List';
import LoginForm from './components/users/LoginForm';
import { useEffect, useMemo, useState } from 'react';
import { RefreshUser } from './behavior/users/api';
import { type UserContextType, UserContext } from './behavior/users/userContext';
import { User } from './behavior/users/types';
import { HandleLoginResponse } from './behavior/users/handlers';
import ConfirmLogout from './components/users/ConfirmLogout';

const App = () => {
  const [currentUser, setCurrentUser] = useState<User | null>(null);

  useEffect(() => {
    const refreshUser = async () => {
      const response = await RefreshUser();
      const user = HandleLoginResponse(response);
      setCurrentUser(user);
    };
    refreshUser();
  }, []);

  const currentUserContext = useMemo<UserContextType>(() => ({
    currentUser: currentUser,
    isLoggedIn: !!currentUser,
    setCurrentUser: setCurrentUser,
  }), [currentUser])

  return (
    <UserContext.Provider value={currentUserContext}>
      <Routes>
        <Route path={WebRoutes.Index} element={<Layout />}>
          <Route index element={<Home />} />
          <Route path={WebRoutes.Briefs.Index}>
            <Route index element={<BriefsList />} />
            <Route path={WebRoutes.Briefs.New} element={<BriefForm />} />
            <Route path={`${WebRoutes.Briefs.Edit}/:briefId`} element={<BriefForm editMode />} />
          </Route>
          <Route path={WebRoutes.User.Login} element={<LoginForm />} />
          <Route path={WebRoutes.User.Logout} element={<ConfirmLogout />} />
          <Route path={WebRoutes.Unmatched} element={<NotFoundPage />} />
        </Route>
      </Routes>
    </UserContext.Provider>
  );
}

export default App;
