import "./Layout.css";
import { Link, Outlet } from "react-router-dom";
import { WebRoutes } from "../../behavior/routes";
import { useCurUserContext } from "../../behavior/users/userContext";

export default function Layout() {
  const { isLoggedIn, currentUser } = useCurUserContext();

  return (
    <>
      <nav>
        <ul>
          <li><Link to={WebRoutes.Index}>На головну</Link></li>
          {isLoggedIn && <li><Link to={WebRoutes.Briefs.Index}>Брифи</Link></li>}
          <li>
            <div className="text-white">{currentUser?.login}</div>
            {isLoggedIn ?
              <Link to={WebRoutes.User.Logout}>Вийти</Link> :
              <Link to={WebRoutes.User.Login}>Увійти</Link>}
          </li>
        </ul>
      </nav>
      <div id='container'>
        <Outlet />
      </div>
    </>
  );
}
