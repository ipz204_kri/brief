import "./NotFoundPage.css";
import { WebRoutes } from '../../behavior/routes';
import { Link } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <div id="notfound">
      <div className="notfound">
        <h2>Oops! This Page Could Not Be Found</h2>
        <p>Sorry but the page you are looking for does not exist, have been removed. name changed or is temporarily unavailable</p>
        <Link to={WebRoutes.Index}>Go To Homepage</Link>
      </div>
    </div>
  );
}
