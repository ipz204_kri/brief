import { AccessorProps, IFormikProps, InputProps } from "./InputRenderers";

interface Props extends IFormikProps, AccessorProps {
  as: (props: InputProps) => React.ReactNode;
  name: string;
  label: string;
  placeholder?: string;
}

export function FormInput({ as, formik, name, label, placeholder, valueAccessor }: Readonly<Props>) {
  return (
    <>
      <label htmlFor={name}>{label}</label>
      {as({
        formik: formik,
        name: name,
        placeholder: placeholder,
        valueAccessor
      })}
    </>
  );
}
