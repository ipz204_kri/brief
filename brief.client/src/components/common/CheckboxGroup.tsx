import { AccessorProps, IFormikProps } from "./InputRenderers";

export type CheckboxInput = { value: string, label: string }[];

interface Props extends IFormikProps, AccessorProps {
  name: string;
  label: string;
  checkboxes: CheckboxInput;
}

export default function CheckBoxGroup({ name, label, checkboxes, formik, valueAccessor }: Readonly<Props>) {
  const IsChecked = (checkboxValue: string) => {
    const values = valueAccessor(formik.values);
    if (Array.isArray(values))
      return values.includes(checkboxValue);
    return false;
  };

  const content = checkboxes.map(checkbox =>
    <label key={checkbox.label}>
      <input
        name={name}
        value={checkbox.value}
        type='checkbox'
        onChange={formik.handleChange}
        checked={IsChecked(checkbox.value)}
      />
      {checkbox.label}
    </label>
  );

  return (
    <>
      <div>{label}</div>
      <fieldset>
        {content}
      </fieldset>
    </>
  );
}
