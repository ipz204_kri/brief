import { FormikProps } from "formik";
import { BriefInfo } from '../../behavior/briefs/types';

export function AsTextarea({ formik, name, valueAccessor, placeholder }: InputProps) {
  return (
    <textarea
      id={name}
      name={name}
      placeholder={placeholder ?? DefaultPlaceholder}
      onChange={formik.handleChange}
      value={valueAccessor(formik.values)}
    />
  );
}

export interface InputProps extends IFormikProps, AccessorProps {
  name: string;
  placeholder?: string;
}

export interface AccessorProps {
  valueAccessor: (data: BriefInfo) => string | string[];
}

export interface IFormikProps {
  formik: FormikProps<BriefInfo>;
}


const DefaultPlaceholder = 'Ваша відповідь';
