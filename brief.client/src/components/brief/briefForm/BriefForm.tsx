import '../../common/Form.css';
import { FormikHelpers, useFormik } from "formik";
import { DefaultBriefDataInput } from '../../../behavior/briefs/constants';
import { BriefInfo } from '../../../behavior/briefs/types';
import { EditBrief, GetBrief, SaveAsPdf, SendBrief } from '../../../behavior/briefs/api';
import BusinessSpecifics from './BusinessSpecifics';
import Competitors from './Competitors';
import TargetAudience from './TargetAudience';
import DesignPreferences from './DesignPreferences';
import Brending from './Brending';
import Content from './Content';
import Opportunities from './Opportunities';
import Navigation from './Navigation';
import MainPage from './MainPage';
import BudgetAndDeadlines from './BudgetAndDeadlines';
import AcceptanceOfWork from './AcceptanceOfWork';
import Deployment from './Deployment';
import { useEffect, useState } from 'react';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import { WebRoutes } from '../../../behavior/routes';
import Customer from './Customer';
import SiteGoals from './SiteGoals';
import { FormatDate } from '../../../behavior/utils';
import { useCurUserContext } from '../../../behavior/users/userContext';
import { useDownloadFile } from '../../../behavior/useDownload';

interface Props {
  editMode?: boolean;
}

export default function BriefForm({ editMode }: Readonly<Props>) {
  const navigate = useNavigate();
  const { briefId } = useParams();
  const { isLoggedIn } = useCurUserContext();
  const formik = useFormik({
    initialValues: DefaultBriefDataInput,
    enableReinitialize: true,
    onSubmit: OnSubmit,
  });

  const [lockSubmitBtn, setLockSubmitBtn] = useState(false);
  const [loading, setLoading] = useState(editMode);
  const [briefDates, setBriefDates] = useState<BriefDate | null>(null);

  useEffect(() => {
    const getBrief = async () => {
      const brief = await GetBrief(briefId!);
      setLoading(true);
      if (brief) {
        formik.resetForm({
          values: brief.data,
        });
        setBriefDates({
          createdDate: brief.createdDate,
          lastModifiedDate: brief.lastModifiedDate,
        });
        setLoading(false);
      }
      else {
        alert('Помилка при читані даних брифу');
        navigate(WebRoutes.Briefs.Index);
      }
    };
    editMode && getBrief();
  }, [editMode]);

  async function OnSubmit(values: BriefInfo, { setSubmitting }: FormikHelpers<BriefInfo>) {
    setLockSubmitBtn(true);

    const response = editMode ? await EditBrief(briefId!, values) : await SendBrief(values);
    if (response?.ok)
      alert('Інформацію було успішно збережено.');
    else
      alert('Сталася помилка при збережені даних.');

    setSubmitting(false);
    navigate(WebRoutes.Index);
  }

  const { ref, url, download, name, isLoadingPdf } = useDownloadFile({
    apiDefinition: async () => await SaveAsPdf(briefId!),
    onError: () => alert('Сталася помилка при завантажені файлу'),
    getFileName: () => FormatDate() + "_brief.pdf"
  });

  if (editMode && !isLoggedIn)
    return <Navigate to={WebRoutes.Index} />

  return loading ? <>Завантаження</> :
    <form onSubmit={formik.handleSubmit}>
      {editMode &&
        <>
          <div className='to-right'>
            <a href={url} download={name} ref={ref} className='hidden'>Download link</a>
            <input type='button' value="Завантажити як PDF" onClick={download} className='save-pdf' />
          </div>
          {isLoadingPdf && <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>}
        </>
      }
      {
        briefDates && <div className='flex'>
          <div>Created date: {FormatDate(briefDates.createdDate)}</div>
          <div>Last modified date: {FormatDate(briefDates.lastModifiedDate)}</div>
        </div>
      }
      <Customer formik={formik} />
      <SiteGoals formik={formik} />
      <BusinessSpecifics formik={formik} />
      <Competitors formik={formik} />
      <TargetAudience formik={formik} />
      <DesignPreferences formik={formik} />
      <Brending formik={formik} />
      <Content formik={formik} />
      <Opportunities formik={formik} />
      <Navigation formik={formik} />
      <MainPage formik={formik} />
      <BudgetAndDeadlines formik={formik} />
      <AcceptanceOfWork formik={formik} />
      <Deployment formik={formik} />
      <button type='submit' disabled={lockSubmitBtn}>Надіслати</button>
    </form >
}

interface BriefDate {
  createdDate: string;
  lastModifiedDate: string;
}
