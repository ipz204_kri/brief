import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function BusinessSpecifics({ formik }: IFormikProps) {
  const FieldNamePrefix = 'BusinessSpecifics';

  return (
    <>
      <h2>Особливості бізнесу</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        name={`${FieldNamePrefix}.Niche`}
        valueAccessor={data => data.BusinessSpecifics.Niche}
        label='Вкажіть особливості бізнес-ніші, в якій ви працюєте. Що визначає конкуренцію в ніші?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.BusinessSpecifics.GrowthPoints}
        name={`${FieldNamePrefix}.GrowthPoints`}
        label='Розкажіть про точки зростання, які можуть бути у вашого бізнесу в майбутньому.'
      />
    </>
  );
}
