import { FormInput } from '../../common/FormInput';
import { AsTextarea, IFormikProps } from '../../common/InputRenderers';

export default function Customer({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Customer';

  return (
    <>
      <h2>Замовник</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Customer}
        name={FieldNamePrefix}
        label='Хто замовник? Це організація чи конкретна особа?'
      />
    </>
  );
}
