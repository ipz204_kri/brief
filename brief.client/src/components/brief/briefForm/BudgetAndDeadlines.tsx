import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function BudgetAndDeadlines({ formik }: IFormikProps) {
  const FieldNamePrefix = 'BudgetAndDeadlines';

  return (
    <>
      <h2>Бюджет і терміни</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.BudgetAndDeadlines.BudgetAndDeadlines}
        name={`${FieldNamePrefix}.BudgetAndDeadlines`}
        label='Визначте бюджет та терміни проєкту (з зазначенням конкретних етапів, які потрібно закінчити до певного часу).'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.BudgetAndDeadlines.HelpAfterRelease}
        name={`${FieldNamePrefix}.HelpAfterRelease`}
        label='Чи потрібна вам підтримка після запуску сайту? Яку саме, як довго потрібно надавати підтримку?'
      />
    </>
  );
}
