import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Content({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Content';

  return (
    <>
      <h2>Контент</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Content.Copyright}
        name={`${FieldNamePrefix}.Copyright`}
        label='Чи є у вас копірайтер, який міг би підготувати текстовий контент для сайту?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Content.ClientMaterials}
        name={`${FieldNamePrefix}.ClientMaterials`}
        label='Чи можна використовувати на сайті інформацію з інших ваших матеріалів?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Content.PaidOrFree}
        name={`${FieldNamePrefix}.PaidOrFree`}
        label='Вкажіть, чи можемо ми брати фото, ілюстрації та відео з платних стоків або тільки з безкоштовних?'
      />
    </>
  );
}
