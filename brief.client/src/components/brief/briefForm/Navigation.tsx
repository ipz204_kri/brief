import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Navigation({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Navigation';

  return (
    <>
      <h2>Навігація та карта сайту</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Navigation.ClientPath}
        name={`${FieldNamePrefix}.ClientPath`}
        label='Яким має бути шлях користувача (наприклад, він повинен отримати максимум інформації про нас або якнайшвидше переходити до цільової дії)?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Navigation.Pages}
        name={`${FieldNamePrefix}.Pages`}
        label="Які сторінки на сайті обов'язкові? Які з них мають бути унікальними, а які відрізнятимуться лише за наповненням"
        placeholder='Приклад: Про нас (унікальна), команда (унікальна), сторінка послуги "Експрес-курс", сторінка послуги "Інтенсив" (типова, структурно така сама як “Експрес-курс”, але з іншим контентом).'
      />
    </>
  );
}
