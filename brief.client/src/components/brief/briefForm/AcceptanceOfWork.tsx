import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function AcceptanceOfWork({ formik }: IFormikProps) {
  const FieldNamePrefix = 'AcceptanceOfWork';

  return (
    <>
      <h2>Прийом робіт</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.AcceptanceOfWork.ContactPerson}
        name={`${FieldNamePrefix}.ContactPerson`}
        label='Визначте відповідальних за кожний етап прийому робіт? Вкажіть їх контактні дані та посади.'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.AcceptanceOfWork.ContactStages}
        name={`${FieldNamePrefix}.ContactStages`}
        label="Скажіть скільки етапів правок ви хотіли б бачити? Після яких кроків розробки сайту ви хотіли б давати зворотний зв'язок?"
      />
    </>
  );
}
