import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Opportunities({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Opportunities';

  return (
    <>
      <h2>Можливості</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Opportunities.List}
        name={`${FieldNamePrefix}.List`}
        label='Перерахуйте можливості, які мають бути на сайті. Ваш сайт має бути однією мовою чи потрібна багатомовність?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Opportunities.ReadyIntegrations}
        name={`${FieldNamePrefix}.ReadyIntegrations`}
        label='Чи є необхідність у готових інтеграціях або сторонніх віджетах? Що саме вас цікавить?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Opportunities.FeaturesToContactWithClient}
        name={`${FieldNamePrefix}.FeaturesToContactWithClient`}
        label="Чи потрібно додавати на сайт форми зворотного зв'язку, попапи, підписку на новини, модуль інтернет-магазину, файли для завантаження, FAQ?"
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Opportunities.Blog}
        name={`${FieldNamePrefix}.Blog`}
        label="Вам потрібен блог? Додати стрічку публікацій на головну, розміщувати посилання на блог в підвалі чи шапці сайту?"
      />
    </>
  );
}
