import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Competitors({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Competitors';

  return (
    <>
      <h2>Конкуренти</h2>
      <FormInput
        formik={formik}
        as={AsTextarea}
        valueAccessor={data => data.Competitors.Uniqueness}
        name={`${FieldNamePrefix}.Uniqueness`}
        label='Розкажіть про ваших конкурентів. Що унікального пропонуєте ви у порівнянні з конкурентами, які переваги мають конкуренти?'
      />
      <FormInput
        formik={formik}
        as={AsTextarea}
        name={`${FieldNamePrefix}.Examples`}
        valueAccessor={data => data.Competitors.Examples}
        label='Наведіть сайти конкурентів, які вам подобаються. Опишіть, що вам подобається і не подобається у сайтах конкурентів?'
      />
      <FormInput
        formik={formik}
        as={AsTextarea}
        valueAccessor={data => data.Competitors.Features}
        name={`${FieldNamePrefix}.Features`}
        label='Наведіть сайти конкурентів, які вам подобаються. Опишіть, що вам подобається і не подобається у сайтах конкурентів?'
      />
    </>
  );
}
