import CheckBoxGroup from "../../common/CheckboxGroup";
import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function DesignPreferences({ formik }: IFormikProps) {
  const FieldNamePrefix = 'DesignPreferences';

  return (
    <>
      <h2>Уподобання в дизайні</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.DesignPreferences.BrendElements}
        name={`${FieldNamePrefix}.BrendElements`}
        label='Скажіть чи є конкретні кольори, шрифти або типи зображень, які повинні бути на сайті?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.DesignPreferences.LikeAndDoNotLikeExamples}
        name={`${FieldNamePrefix}.LikeAndDoNotLikeExamples`}
        label='Наведіть приклади сайтів чи стилів у дизайні, які вам подобаються або не подобаються. Чому вони вам подобаються або не подобаються?'
      />
      <CheckBoxGroup
        formik={formik}
        valueAccessor={data => data.DesignPreferences.Emotions}
        name={`${FieldNamePrefix}.Emotions`}
        label='Який настрій має викликати дизайн вашого сайту? Перерахуйте ці емоції.'
        checkboxes={[
          { label: 'Суворий корпоративний стиль', value: 'Strict' },
          { label: 'Яскравий, помітний дизайн', value: 'Bright' },
          { label: 'Позитивний і веселий дизайн', value: 'Positive' },
          { label: 'Дизайн насичений зображеннями', value: 'WithPictures' },
          { label: 'Динамічний сайт: анімації та відеофони', value: 'Dynamic' },
          { label: 'Статичний сайт: без анімацій та відволікаючих елементів', value: 'Static' },
          { label: 'Дизайн з акцентом на мінімалізм', value: 'Minimalism' },
          { label: 'Дизайн з акцентом на функціональність', value: 'Functionality' },
        ]}
      />
    </>
  );
}
