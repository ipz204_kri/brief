import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Brending({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Brending';

  return (
    <>
      <h2>Брендинг</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Brending.BrendBook}
        name={`${FieldNamePrefix}.BrendBook`}
        label='Чи є у вас брендбук? Наскільки він актуальний, наскільки потрібно дотримуватись його вимог?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Brending.Ideas}
        name={`${FieldNamePrefix}.Ideas`}
        label='Чи є у вас ідеї щодо того, як сайт повинен відображати індивідуальність бренду та послання бренду? Розкажіть як саме.'
      />
    </>
  );
}
