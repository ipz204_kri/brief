import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function MainPage({ formik }: IFormikProps) {
  const FieldNamePrefix = 'MainPage';

  return (
    <>
      <h2>Головна сторінка</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.MainPage.MinimalisticOrDetailed}
        name={`${FieldNamePrefix}.MinimalisticOrDetailed`}
        label='Якою має бути головна сторінка: мінімалістичною чи докладною?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.MainPage.Functions}
        name={`${FieldNamePrefix}.Functions`}
        label='Які функції потрібно реалізувати на головній сторінці (наприклад, перехід до магазину, надсилання форм, детальний опис послуг)?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.MainPage.Content}
        name={`${FieldNamePrefix}.Content`}
        label='Що потрібно розмістити на головній сторінці сайту?'
      />
    </>
  );
}
