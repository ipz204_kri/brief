import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function Deployment({ formik }: IFormikProps) {
  const FieldNamePrefix = 'Deployment';

  return (
    <>
      <h2>Запуск сайту</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Deployment.IsDeployNeeded}
        name={`${FieldNamePrefix}.IsDeployNeeded`}
        label='Ви хотіли б отримати сайт, готовий до запуску, або вже запущений сайт?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Deployment.SEO}
        name={`${FieldNamePrefix}.SEO`}
        label='Чи буде SEO на сайті? Коли ви готові надати всі необхідні елементи контенту для оптимізації SEO?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.Deployment.Domain}
        name={`${FieldNamePrefix}.Domain`}
        label='У вас є домен для сайту чи його потрібно купити? Який має бути домен, якщо його потрібно купувати?'
      />
    </>
  );
}
