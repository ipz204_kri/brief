import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function TargetAudience({ formik }: IFormikProps) {
  const FieldNamePrefix = 'TargetAudience';

  return (
    <>
      <h2>Цільова аудиторія</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.TargetAudience.DemographicData}
        name={`${FieldNamePrefix}.DemographicData`}
        label='Визначте демографічні дані та характеристики цільової аудиторії сайту: вік, стать, географію та інтереси.'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.TargetAudience.PerfectClient}
        name={`${FieldNamePrefix}.PerfectClient`}
        label='Якщо у вас є портрет ідеального клієнта, розкажіть про нього детальніше.'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.TargetAudience.ProblemsAndMotives}
        name={`${FieldNamePrefix}.ProblemsAndMotives`}
        label='Якщо у вас є портрет ідеального клієнта, розкажіть про нього детальніше.'
      />
    </>
  );
}
