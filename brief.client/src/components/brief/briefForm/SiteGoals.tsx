import CheckBoxGroup from "../../common/CheckboxGroup";
import { FormInput } from "../../common/FormInput";
import { AsTextarea, IFormikProps } from "../../common/InputRenderers";

export default function SiteGoals({ formik }: IFormikProps) {
  const FieldNamePrefix = 'SiteGoals';

  return (
    <>
      <h2>Завдання сайту</h2>
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.SiteGoals.HaveWebSite}
        name={`${FieldNamePrefix}.HaveWebSite`}
        label='Чи є у вас зараз сайт? Що вам подобається і не подобається у ньому?'
      />
      <FormInput
        as={AsTextarea}
        formik={formik}
        valueAccessor={data => data.SiteGoals.FinalGoals}
        name={`${FieldNamePrefix}.FinalGoals`}
        label='Яка кінцева мета вашого сайту? (наприклад, прямі продажі, надання інформації про компанію, використання як вітрини товарів)'
      />
      <CheckBoxGroup
        formik={formik}
        valueAccessor={data => data.SiteGoals.AdditionalGoals}
        name={`${FieldNamePrefix}.AdditionalGoals`}
        label='Якщо є додаткові цілі сайтів, вкажіть їх.'
        checkboxes={[
          { label: 'Покращення логічної структури сайту', value: 'LogicalStructure' },
          { label: 'Залучення більшої кількості клієнтів', value: 'MoreClients' },
          { label: 'Впровадження сучасних трендів дизайну', value: 'ModernDesign' },
          { label: 'Створення багатомовного сайту', value: 'MultiLanguage' },
          { label: 'Збільшення впізнаваності бренду', value: 'BrendRecognition' },
          { label: 'Просування нового продукту або послуги', value: 'ProductPromotion' },
          { label: 'Збільшення кількості продажів', value: 'SalesIncrease' },
          { label: 'Інформування (інформаційна мета)', value: 'Informing' },
          { label: 'Ребрендинг або редизайн попереднього сайту', value: 'Rebrending' },
          { label: 'SEO-оптимізація сайту, усунення помилок', value: 'SEO' },
        ]}
      />
    </>
  );
}
