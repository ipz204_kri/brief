import "./styles.css";
import { useEffect, useState } from "react";
import { useCurUserContext } from "../../../behavior/users/userContext";
import { Navigate } from "react-router-dom";
import { WebRoutes } from "../../../behavior/routes";
import { GetBriefs } from "../../../behavior/briefs/api";
import { Brief, MapResponseToBrief } from "../../../behavior/briefs/types";
import BriefRow from "./Row";

const BriefsList = () => {
  const { isLoggedIn } = useCurUserContext();

  const [isLoading, setIsLoading] = useState(true);
  const [briefs, setBriefs] = useState<(Brief | null)[]>([]);

  useEffect(() => {
    const loadBriefs = async () => {
      const response = await GetBriefs();
      if (Array.isArray(response)) {
        const briefs = response.map(b => MapResponseToBrief(b));
        setBriefs(briefs);
      }
      else {
        alert("Сталася помилка при завантаженні брифів");
      }
      setIsLoading(false);
    };

    isLoading && loadBriefs();
  }, [isLoading]);

  if (!isLoggedIn)
    return <Navigate to={WebRoutes.Index} />

  return isLoading ?
    <div>Завантаження</div> :
    <div className="table-container">
      <table className="brief-table">
        <thead>
          <tr>
            <th>Customer</th>
            <th>Created Date</th>
            <th>Last Modified Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {briefs.map(brief => (brief &&
            <BriefRow key={brief.id} brief={brief} setUpdateList={setIsLoading} />
          ))}
        </tbody>
      </table>
    </div>
};

export default BriefsList;
