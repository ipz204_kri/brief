import { Brief } from "../../../behavior/briefs/types";
import { FaEdit, FaTrash } from 'react-icons/fa';
import { FormatDate } from '../../../behavior/utils';
import { DeleteBriefInfo } from "../../../behavior/briefs/api";
import { useNavigate } from "react-router-dom";
import { WebRoutes } from "../../../behavior/routes";

interface Props {
  brief: Brief;
  setUpdateList: (shouldUpdate: boolean) => void;
}

const BriefRow = ({ brief, setUpdateList }: Props) => {
  const navigate = useNavigate();

  const OnDeleteClick = async () => {
    await DeleteBriefInfo(brief.id);
    setUpdateList(true);
  };

  const OnEditClick = () => {
    navigate(`${WebRoutes.Briefs.Edit}/${brief.id}`);
  };

  return (
    <tr>
      <td>{brief.data.Customer}</td>
      <td>{FormatDate(brief.createdDate)}</td>
      <td>{FormatDate(brief.lastModifiedDate)}</td>
      <td className="action-icons">
        <FaEdit className="edit-icon" onClick={OnEditClick} />
        <FaTrash className="delete-icon" onClick={OnDeleteClick} />
      </td>
    </tr>
  );
};

export default BriefRow;
