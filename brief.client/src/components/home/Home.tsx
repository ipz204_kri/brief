import "./Home.css";
import { WebRoutes } from "../../behavior/routes";
import { Link } from "react-router-dom";

const Home = () =>
  <div className="container">
    <div className="inline-vertical">
      <h1>Вітаємо!</h1>
      <Link to={WebRoutes.Briefs.New} className="btn">Створити запит на розробку вебсайту</Link>
    </div>
  </div>

export default Home;
