import { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { WebRoutes } from "../../behavior/routes";
import { Logout } from "../../behavior/users/api";
import { useCurUserContext } from "../../behavior/users/userContext";

export default function ConfirmLogout() {
  const navigate = useNavigate();
  const { isLoggedIn, setCurrentUser } = useCurUserContext();

  const [blockBtns, setBlockBtns] = useState(false);

  const OnDeny = () => navigate(-1);

  const OnConfirm = async () => {
    setBlockBtns(true);
    await Logout();
    setCurrentUser(null);
    navigate(WebRoutes.Index);
  };

  if (!isLoggedIn)
    return <Navigate to={WebRoutes.Index} />

  return (
    <>
      <h2 className="text-center">Ви впевнені що хочете вийти з облікового запису?</h2>
      <div className="confirm-container">
        <button onClick={OnDeny} disabled={blockBtns} className="btn deny" >Назад</button>
        <button onClick={OnConfirm} className="btn confirm">Вийти</button>
      </div>
    </>
  );
}
