import '../common/Form.css';
import type { LoginInput } from '../../behavior/users/types';
import { useState } from 'react';
import { FormikHelpers, useFormik } from "formik";
import { Login } from '../../behavior/users/api';
import { HandleLoginResponse } from '../../behavior/users/handlers';
import { Navigate, useNavigate } from "react-router-dom";
import { WebRoutes } from '../../behavior/routes';
import { useCurUserContext } from '../../behavior/users/userContext';

export default function LoginForm() {
  const navigate = useNavigate();
  const { isLoggedIn, setCurrentUser } = useCurUserContext();
  const formik = useFormik({
    initialValues: InitialLoginValues,
    onSubmit: OnSubmit,
  });

  const [lockSubmitBtn, setLockSubmitBtn] = useState(false);
  const [error, setError] = useState<string | null>(null);

  async function OnSubmit(values: LoginInput, { setSubmitting }: FormikHelpers<LoginInput>) {
    if (values.login === '' || values.password === '') {
      setError('Поля не можуть бути пустими');
      return;
    }

    setLockSubmitBtn(true);

    const response = await Login(values);

    const user = HandleLoginResponse(response);
    if (user) {
      setError(null);
      navigate(WebRoutes.Briefs.Index);
      setCurrentUser(user);
    }
    else {
      setError('Користувача з таким логіном та паролем не існує')
    }

    setLockSubmitBtn(false);
    setSubmitting(false);
  }

  if (isLoggedIn)
    return <Navigate to={WebRoutes.Briefs.Index} />

  return (
    <form className='small' onSubmit={formik.handleSubmit}>
      <h3>Введіть дані облікогового запису</h3>
      <label htmlFor='login'>Логін</label>
      <input
        name='login'
        placeholder='login'
        value={formik.values.login}
        onChange={formik.handleChange}
      />
      <label htmlFor='password'>Пароль</label>
      <input
        name='password'
        placeholder='password'
        type='password'
        value={formik.values.password}
        onChange={formik.handleChange}
      />
      <button type='submit' disabled={lockSubmitBtn}>Увійти</button>
      {error && <div className='error'>{error}</div>}
    </form>
  );
}

const InitialLoginValues: LoginInput = {
  login: '',
  password: '',
};
