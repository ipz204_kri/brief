import { Brief, BriefInfo } from "./types";

const ApiUrl = `/BriefInfo`;
const FetchOptions = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  credentials: 'include' as RequestCredentials,
};

export async function GetBriefs() {
  try {
    const response = await fetch(`${ApiUrl}/GetBriefsInfo`, {
      method: 'GET',
      ...FetchOptions,
    });

    return response.json();
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export async function GetBrief(id: string) {
  try {
    const response = await fetch(`${ApiUrl}/GetBriefInfo?id=${id}`, {
      method: 'GET',
      ...FetchOptions,
    });

    if (!response.ok)
      return null;

    const data = await response.json();
    return {
      id: data.id,
      data: JSON.parse(data.data) as BriefInfo,
      createdDate: data.createdDate,
      lastModifiedDate: data.lastModifiedDate,
    } as Brief;
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export async function SendBrief(data: BriefInfo) {
  try {
    const response = await fetch(ApiUrl, {
      method: 'POST',
      body: JSON.stringify(data),
      ...FetchOptions,
    });

    return response;
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export async function EditBrief(id: string, data: BriefInfo) {
  try {
    const response = await fetch(`${ApiUrl}/EditBriefInfo`, {
      method: 'POST',
      body: JSON.stringify({ Id: id, Data: data }),
      ...FetchOptions,
    });

    return response;
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}


export async function DeleteBriefInfo(id: string) {
  try {
    await fetch(`${ApiUrl}`, {
      method: 'DELETE',
      body: JSON.stringify({ BriefId: id }),
      ...FetchOptions,
    });
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export const SaveAsPdf = async (id: string) => await fetch(`${ApiUrl}/SaveAsPdf`, {
  method: 'POST',
  body: JSON.stringify({ BriefId: id }),
  headers: {
    'Content-Type': 'application/json',
  },
});
