export interface Brief {
  id: string;
  data: BriefInfo;
  createdDate: string;
  lastModifiedDate: string;
}

export interface BriefResponse {
  id: string;
  data: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface BriefInfo {
  Customer: string;
  SiteGoals: {
    HaveWebSite: string;
    FinalGoals: string;
    AdditionalGoals: string[];
  };
  BusinessSpecifics: {
    Niche: string;
    GrowthPoints: string;
  };
  Competitors: {
    Uniqueness: string;
    Examples: string;
    Features: string;
  };
  TargetAudience: {
    DemographicData: string;
    PerfectClient: string;
    ProblemsAndMotives: string;
  };
  DesignPreferences: {
    BrendElements: string;
    LikeAndDoNotLikeExamples: string;
    Emotions: string[];
  };
  Brending: {
    BrendBook: string;
    Ideas: string;
  };
  Content: {
    Copyright: string;
    ClientMaterials: string;
    PaidOrFree: string;
  };
  Opportunities: {
    List: string;
    ReadyIntegrations: string;
    FeaturesToContactWithClient: string;
    Blog: string;
  };
  Navigation: {
    ClientPath: string;
    Pages: string;
  };
  MainPage: {
    MinimalisticOrDetailed: string;
    Functions: string;
    Content: string;
  };
  BudgetAndDeadlines: {
    BudgetAndDeadlines: string;
    HelpAfterRelease: string;
  };
  AcceptanceOfWork: {
    ContactPerson: string;
    ContactStages: string;
  };
  Deployment: {
    IsDeployNeeded: string;
    SEO: string;
    Domain: string;
  };
}

export const MapResponseToBrief = (value: BriefResponse): Brief | null => {
  try {
    return {
      id: value?.id,
      data: JSON.parse(value.data),
      createdDate: value.createdDate,
      lastModifiedDate: value.lastModifiedDate
    };
  }
  catch {
    return null;
  }
};
