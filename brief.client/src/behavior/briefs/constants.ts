import { BriefInfo } from "./types";

export const DefaultBriefDataInput: BriefInfo = {
  Customer: '',
  SiteGoals: {
    HaveWebSite: '',
    FinalGoals: '',
    AdditionalGoals: [],
  },
  BusinessSpecifics: {
    Niche: '',
    GrowthPoints: '',
  },
  Competitors: {
    Uniqueness: '',
    Examples: '',
    Features: '',
  },
  TargetAudience: {
    DemographicData: '',
    PerfectClient: '',
    ProblemsAndMotives: '',
  },
  DesignPreferences: {
    BrendElements: '',
    LikeAndDoNotLikeExamples: '',
    Emotions: [],
  },
  Brending: {
    BrendBook: '',
    Ideas: '',
  },
  Content: {
    Copyright: '',
    ClientMaterials: '',
    PaidOrFree: '',
  },
  Opportunities: {
    List: '',
    ReadyIntegrations: '',
    FeaturesToContactWithClient: '',
    Blog: '',
  },
  Navigation: {
    ClientPath: '',
    Pages: '',
  },
  MainPage: {
    MinimalisticOrDetailed: '',
    Functions: '',
    Content: '',
  },
  BudgetAndDeadlines: {
    BudgetAndDeadlines: '',
    HelpAfterRelease: '',
  },
  AcceptanceOfWork: {
    ContactPerson: '',
    ContactStages: '',
  },
  Deployment: {
    IsDeployNeeded: '',
    SEO: '',
    Domain: '',
  },
};
