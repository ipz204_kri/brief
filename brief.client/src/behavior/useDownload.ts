import { useRef, useState } from "react";

interface DownloadFileProps {
  readonly apiDefinition: () => Promise<Response>;
  readonly onError: () => void;
  readonly getFileName: () => string;
}

interface DownloadedFileInfo {
  readonly download: () => Promise<void>;
  readonly ref: React.MutableRefObject<HTMLAnchorElement | null>;
  readonly name: string | undefined;
  readonly url: string | undefined;
  readonly isLoadingPdf: boolean;
}

export const useDownloadFile = ({
  apiDefinition,
  onError,
  getFileName,
}: DownloadFileProps): DownloadedFileInfo => {
  const ref = useRef<HTMLAnchorElement | null>(null);
  const [url, setFileUrl] = useState<string>();
  const [name, setFileName] = useState<string>();
  const [isLoadingPdf, setIsLoadingPdf] = useState<boolean>(false);

  const download = async () => {
    try {
      setIsLoadingPdf(true);
      const response = await apiDefinition();
      const blob = await response.blob();
      const url = URL.createObjectURL(blob);
      setFileUrl(url);
      setFileName(getFileName());
      ref.current?.click();
      //URL.revokeObjectURL(url);
      setIsLoadingPdf(false);
    } catch (error) {
      onError();
    }
  };

  return { download, ref, url, name, isLoadingPdf };
};
