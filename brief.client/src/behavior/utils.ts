export function FormatDate(date?: string) {
  return date ? new Date(date).toLocaleString() : new Date().toLocaleString();
}
