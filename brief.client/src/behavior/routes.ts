export const WebRoutes = {
  Index: '/',
  Briefs: {
    Index: '/briefs',
    New: '/briefs/new',
    Edit: '/briefs/edit',
  },
  User: {
    Login: '/user/login',
    Logout: '/user/logout',
  },
  Unmatched: '*',
};
