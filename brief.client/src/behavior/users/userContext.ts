import type { User } from "./types";
import { createContext, useContext } from "react";

export interface UserContextType {
  currentUser: User | null;
  setCurrentUser: (user: User | null) => void;
  isLoggedIn: boolean;
}

const DefaultUserContext: UserContextType = {
  currentUser: null,
  setCurrentUser: () => { },
  isLoggedIn: false,
};

export const UserContext = createContext<UserContextType>(DefaultUserContext);

export const useCurUserContext = () => useContext(UserContext);
