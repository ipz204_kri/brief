import { User } from "./types";

export function HandleLoginResponse(response: LoginResponse | null) {
  let user: User | null = null;

  if (!response)
    return user;

  if (response.id && response.login) {
    user = {
      id: response.id,
      login: response.login,
    };
  }

  return user;
}

interface LoginResponse {
  status?: number;
  title?: string;
  id?: string;
  login?: string;
}
