import { LoginInput } from "./types";

const ApiBaseUrl = `/User`;
const FetchOptions = {
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  credentials: 'include' as RequestCredentials,
};

export async function Login(data: LoginInput) {
  try {
    const response = await fetch(`${ApiBaseUrl}/Login`, {
      method: 'POST',
      body: JSON.stringify(data),
      ...FetchOptions,
    });

    return response.json();
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export async function RefreshUser() {
  try {
    const response = await fetch(`${ApiBaseUrl}/RefreshUser`, {
      method: 'POST',
      ...FetchOptions,
    });

    if (response.ok)
      return response.json();

    return null;
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}

export async function Logout() {
  try {
    await fetch(`${ApiBaseUrl}/Logout`, {
      method: 'POST',
      ...FetchOptions,
    });
  }
  catch (err) {
    console.error(err);
    if (err instanceof Error)
      alert(err.message);
  }
}
